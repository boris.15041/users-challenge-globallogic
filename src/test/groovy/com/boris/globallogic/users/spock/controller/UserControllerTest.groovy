package com.boris.globallogic.users.spock.controller

import com.boris.globallogic.users.controller.UserController
import com.boris.globallogic.users.services.IUserService
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Ignore

import static com.boris.globallogic.users.mock.UsersData.*
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

import static org.mockito.Mockito.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc;

    @Autowired(required = false)
    private UserController userController;

    @MockBean
    private IUserService userService;

    def 'when context is loaded then all expected beans are created'() {
        expect: 'the WebController is created'
        userController
    }

    void 'Find all users from controller with status 200 OK in the response'() {

        ObjectMapper objectMapper = new ObjectMapper();

        given: 'Users list from mock'
            def users = findAllUsers();
        when: 'Get all users from service'
            def mock = when(userService.findAll()).thenReturn(users)

        then: 'Call endpoint GET /api/users'
            def result = mockMvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath('$[0].name').value(NAME_USER_001 + " " + LAST_NAME_USER_001))
                    .andExpect(jsonPath('$[0].email').value(EMAIL_USER_001))
                    .andExpect(jsonPath('$[1].name').value(NAME_USER_002 + " " + LAST_NAME_USER_002))
                    .andExpect(jsonPath('$[1].email').value(EMAIL_USER_002))
                    .andExpect(jsonPath('$', Matchers.hasSize(2)))
                    .andExpect(content().json(objectMapper.writeValueAsString(findAllUsers())));

    }

}
