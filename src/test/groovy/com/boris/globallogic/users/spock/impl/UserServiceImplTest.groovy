package com.boris.globallogic.users.spock.impl

import com.boris.globallogic.users.exceptions.EntityNotFoundException
import com.boris.globallogic.users.exceptions.UniqueEmailException
import com.boris.globallogic.users.models.entity.User
import com.boris.globallogic.users.spock.data.PhonesData

import static com.boris.globallogic.users.mock.UsersData.*;

import com.boris.globallogic.users.models.repository.UserDAO
import com.boris.globallogic.users.services.IUserService
import com.boris.globallogic.users.utils.ConstantsUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import spock.lang.Specification

import java.time.format.DateTimeFormatter

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertNotNull
import static org.junit.jupiter.api.Assertions.assertThrows
import static org.junit.jupiter.api.Assertions.assertTrue
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.when

@SpringBootTest
class UserServiceImplTest extends Specification {

    public final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConstantsUtils.FORMATTER_DATE);
    public final String ID_USER = "ff8081817ade0f4b017a2632665599266";

    @MockBean
    private UserDAO userDAO;

    @Autowired
    private IUserService userService;

    void 'Buscar todos los usuarios desde el servicio'() {

        given: 'Mock de usuarios de la base de datos de prueba para la llamada al repositorio y total de usuarios activos'
            def mockUsers = findAllUsersIsActive();
            when(userDAO.findAllByIsActiveIsTrue()).thenReturn(mockUsers);
            def totalActiveUsers = 2;

        when: 'Se obtiene los usuarios desde el servicio'
            def usersDTO =  userService.findAll();

        then: 'La cantidad de elementos deben ser iguales y los datos de cada elemento deben coincidir con el mock'
            assertEquals(2, usersDTO.size());
            assertNotNull(usersDTO);
            assertNotNull(usersDTO.get(0).getPhones());
            assertNotNull(usersDTO.get(1).getPhones());
            assertEquals(NAME_USER_001 + " " + LAST_NAME_USER_001, usersDTO.get(0).getName());
            assertEquals(EMAIL_USER_001, usersDTO.get(0).getEmail());
            assertEquals(2, usersDTO.get(0).getPhones().size());
            assertEquals(NAME_USER_002 + " " + LAST_NAME_USER_002, usersDTO.get(1).getName());
            assertEquals(EMAIL_USER_002, usersDTO.get(1).getEmail());
            assertEquals(3, usersDTO.get(1).getPhones().size());

    }

    void 'Buscar un usuario por su ID y no existe en la base de datos'() {

        given: 'ID inexistente de un usuario y devolución vacía del repositorio'
            def userId = ID_USER;

        when: 'Se llama al repositorio y devuelve un resultado vacio al no existir la base de datos'
            when(userDAO.findById(userId)).thenReturn(Optional.empty());

        then: 'Se lanza la excepción de entidad no encontrada'
            assertThrows(EntityNotFoundException.class, () -> {
                userService.findById(ID_USER);
            });

    }

    void 'Buscar un usuario existente por su ID'() {

        given: 'ID de un usuario existente en la base de datos de prueba. Resultado de la llamada al repositorio'
            def userId = ID_USER_001;
            when(userDAO.findById(userId)).thenReturn(getUser001());

        when: 'Se llama al metodo del servicio para buscar al usuario por su ID'
            def userFound = userService.findById(userId).get();

        then: 'La respuesta no debe ser nula, los datos del usuario encontrado deben coincidir con el mock'
            assertNotNull(userFound);
            assertEquals(NAME_USER_001 + " " + LAST_NAME_USER_001, userFound.getName());
            assertEquals(EMAIL_USER_001, userFound.getEmail());
            assertEquals(2, userFound.getPhones().size());

    }

    void 'Al guardar un nuevo usuario debe tener un email único en el sistema'() {

        given: 'Email que ya existe en la base de datos'
            def email = EMAIL_USER_001;

        when: 'Se busca si ya existe el email del nuevo usuario y retorna el usuario con el email existente'
            when(userDAO.findByEmail(EMAIL_USER_001)).thenReturn(getUser001());

        then: 'Se lanza una excepción de Email Existente'
            assertThrows(UniqueEmailException.class, () -> {
                userService.save(createUser001().get());
            });

    }

    void 'Se guarda un nuevo usuario correctamente'() {

        given: 'Email inexistente en la base de datos. El repositorio devuelve un valor vacio'
            def email = EMAIL_USER_001;
            when(userDAO.findByEmail(email)).thenReturn(Optional.empty());

        when: 'Se guarda el nuevo usuario en la base de datos con los metodos del servicio y repositorio'
            when(userDAO.save(any())).then(invocation -> {
                User user = invocation.getArgument(0, User.class);
                user.setId(ID_USER_001);
                user.setName(NAME_USER_001);
                user.setEmail(EMAIL_USER_001);
                user.setPassword(PASSWORD_USER_001);
                user.setToken(TOKEN_USER_001);
                user.setCreated(CREATED_USER_001);
                user.setIsActive(true);
                user.setPhones(PhonesData.getPhonesUser001());
                return user;
            });
            def userSuccess = userService.save(createUser001().get()).get();

        then: 'El resultado no puede ser null, los datos devueltos del servicio deben coincidir con los datos de entrada'
            assertNotNull(userSuccess);
            assertEquals(ID_USER_001, userSuccess.getId());
            assertEquals(CREATED_USER_001.format(formatter), userSuccess.getCreated().format(formatter));
            assertNotNull(userSuccess.getLastLogin());
            assertEquals(TOKEN_USER_001, userSuccess.getToken());
            assertTrue(userSuccess.getIsActive());

    }


}
