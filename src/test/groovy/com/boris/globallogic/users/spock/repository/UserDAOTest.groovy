package com.boris.globallogic.users.spock.repository

import com.boris.globallogic.users.models.entity.User
import com.boris.globallogic.users.models.repository.UserDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

import java.time.LocalDateTime
import static com.boris.globallogic.users.mock.UsersData.*
import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertNotNull
import static org.junit.jupiter.api.Assertions.assertThrows
import static org.junit.jupiter.api.Assertions.assertTrue

@DataJpaTest
class UserDAOTest extends Specification {

    public final String EMAIL_USER = "example@gmail.com";

    public final String ID_USER_NEW = "ff8081817ade0f4b0o0s9o99dks990";
    public final String NAME_USER_NEW = "Agustin Emiliano";
    public final String LAST_NAME_USER_NEW = "Romero";
    public final String EMAIL_USER_NEW = "agustin@gmail.com";
    public final String PASSWORD_USER_NEW = "662asDD2";
    public final String TOKEN_USER_NEW = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoiZ2xvYmFsTG9naWMiLCJleHAiOjkwLCJpYXQiOjE2MjcyMTQ4ODUsImVtYWlsIjoicm91OXMzQGdtYWlsLmNvbSIsImp0aSI6IjFkZDM4NDM3LTJiNzEtNGQ4Yy04ZjhiLTNhZWUxM2QwOWIwOSJ9.Ym5KAV7W8qs4mUYuBK8dPmCGl_ikc4Rdn5zGrxWRf2k";
    public final LocalDateTime CREATED_USER_NEW = LocalDateTime.of(2021, 07, 23, 20, 15, 30);
    public final LocalDateTime MODIFIED_USER_NEW = LocalDateTime.of(2021, 07, 24, 22, 16, 31);

    @Autowired
    private UserDAO userDAO;

    void 'Buscar un usuario desde la base de datos por su ID'() {

        given: 'ID del usuario a buscar en base de datos'
            def userId = ID_USER_001;
        when: 'Se busca al usuario en la base de datos mediante su ID'
            def user = userDAO.findById(userId);
        then: 'Se encuentra el usuario y su ID debe ser igual al ID que se realizó la búsqueda'
            assertTrue(user.isPresent());
            assertEquals(ID_USER_001, user.get().getId());

    }

    void 'Buscar un usuario desde la base de datos por su email'() {

        given: 'Email del usuario a buscar en base de datos'
            def email = EMAIL_USER_002;
        when: 'Se busca al usuario en la base de datos mediante su email'
            def user = userDAO.findByEmail(email);
        then: 'El usuario se encuentra y su email debe ser igual al email con el que se realizó la búsqueda'
            assertTrue(user.isPresent());
            assertEquals(EMAIL_USER_002, user.get().getEmail());

    }

    void 'Lanzar una excepción cuando se busca a un usuario con un email inexistente'() {

        given: 'Email inexistente en la base de datos para buscar un usuario'
            def email = EMAIL_USER;
        when: 'Se busca al usuario en la base de datos mediante el email'
            def user = userDAO.findByEmail(email);
        then: 'Se lanza una excepciónd e entidad no encontrada y el usuario no está presente'
            assertThrows(NoSuchElementException.class, () -> {
                user.get();
            });
            assertFalse(user.isPresent());

    }

    void 'Obtener todos los usuarios activos desde la base de datos'() {

        given: 'Cantidad de usuarios activos en la base de datos de prueba'
            def totalActiveUsers = 2;
        when: 'Se obtiene los usuarios activos en la base de datos desde el repositorio'
            def users = userDAO.findAllByIsActiveIsTrue();
        then: 'Lo que devuelve el método no debe estar vacío y debe ser igual a los usuarios activos de la base de datos de prueba'
            assertFalse(users.isEmpty());
            assertEquals(2, users.size());

    }

    void 'Guardar un nuevo usuario en la base de datos'() {

        given: 'Nuevo usuario que debe ser registrado en el sistema'
            def user = new User();
            user.setName(NAME_USER_NEW);
            user.setEmail(EMAIL_USER_NEW);
            user.setPassword(PASSWORD_USER_NEW);
            user.setToken(TOKEN_USER_NEW);
            user.setCreated(CREATED_USER_NEW);
            user.setModified(MODIFIED_USER_NEW);
            user.setIsActive(true);

        when: 'Se guarda en la base de datos mediante el método'
            def userRegistered = userDAO.save(user);

        then: 'El id del nuevo usuario no debe ser null, los datos devueltos por el método deben ser igual a los datos de entrada'
            assertNotNull(userRegistered.getId());
            assertEquals(NAME_USER_NEW, userRegistered.getName());
            assertEquals(EMAIL_USER_NEW, userRegistered.getEmail());

    }

}
