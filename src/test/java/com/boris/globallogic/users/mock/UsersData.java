package com.boris.globallogic.users.mock;

import com.boris.globallogic.users.dto.UserDTO;
import com.boris.globallogic.users.dto.UserSuccessDTO;
import com.boris.globallogic.users.dto.UserUnregisteredDTO;
import com.boris.globallogic.users.models.entity.User;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UsersData {

    public static final String ID_USER_001 = "ff8081817ade0f4b017ade103dda0000";
    public static final String NAME_USER_001 = "Rocio Mariana";
    public static final String LAST_NAME_USER_001 = "Rodriguez";
    public static final String EMAIL_USER_001 = "roo.123@gmail.com";
    public static final String PASSWORD_USER_001 = "aaZZa44";
    public static final String TOKEN_USER_001 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoiZ2xvYmFsTG9naWMiLCJleHAiOjkwLCJpYXQiOjE2MjcyMTQ4ODUsImVtYWlsIjoicm91OXMzQGdtYWlsLmNvbSIsImp0aSI6IjFkZDM4NDM3LTJiNzEtNGQ4Yy04ZjhiLTNhZWUxM2QwOWIwOSJ9.Ym5KAV7W8qs4mUYuBK8dPmCGl_ikc4Rdn5zGrxWRf2k";
    public static final LocalDateTime CREATED_USER_001 = LocalDateTime.of(2021, 07, 23, 15, 15, 30);
    public static final LocalDateTime MODIFIED_USER_001 = LocalDateTime.of(2021, 07, 24, 16, 16, 31);
    public static final LocalDateTime LAST_LOGIN_USER_001 = LocalDateTime.of(2021, 07, 25, 17, 17,32);

    public static final String ID_USER_002 = "ff8081817ade0f4b017ade357e2c0001";
    public static final String NAME_USER_002 = "Florencia Ivana";
    public static final String LAST_NAME_USER_002 = "Salas";
    public static final String EMAIL_USER_002 = "flor.ivana.321@gmail.com";
    public static final String PASSWORD_USER_002 = "45DD96ss";
    public static final String TOKEN_USER_002 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoiZ2xvYmFsTG9naWMiLCJleHAiOjkwLCJpYXQiOjE2MjcyMTcwMTksImVtYWlsIjoicm91OTQ0MzIzczNAZ21haWwuY29tIiwianRpIjoiMGQ2NjQ4MDktMmRlZi00NDczLTg2MjMtYWIwMzE3YTMxMmUxIn0.iZi7wo5PAYMHLtUi_HArZ3PTLVZOj2ms3U4xtmMAvNc";

    public static Optional<UserUnregisteredDTO> createUser001() {
        return Optional.of(
                UserUnregisteredDTO.builder()
                    .withName(NAME_USER_001)
                    .withEmail(EMAIL_USER_001)
                    .withPassword(PASSWORD_USER_001)
                    .withPhones(com.boris.globallogic.users.mock.PhonesData.getPhonesUnregisteredDTO001())
                    .build()
                );
    }

    public static Optional<UserSuccessDTO> getUserSuccessDTO001() {
        return Optional.of(
                UserSuccessDTO.builder()
                        .withId(ID_USER_001)
                        .withCreated(CREATED_USER_001)
                        .withLastLogin(LAST_LOGIN_USER_001)
                        .withToken(TOKEN_USER_001)
                        .withIsActive(true)
                        .build()
        );
    }

    public static Optional<User> getUser001() {
        User user = new User();
        user.setId(ID_USER_001);
        user.setName(NAME_USER_001 + " " + LAST_NAME_USER_001);
        user.setEmail(EMAIL_USER_001);
        user.setPassword(PASSWORD_USER_001);
        user.setToken(TOKEN_USER_001);
        user.setIsActive(true);
        user.setPhones(com.boris.globallogic.users.mock.PhonesData.getPhonesUser001());
        return Optional.of(user);
    }

    public static Optional<UserDTO> getUserDTO001() {
        return Optional.of(UserDTO.builder()
            .withName(NAME_USER_001 + " " + LAST_NAME_USER_001)
            .withEmail(EMAIL_USER_001)
            .withPhones(com.boris.globallogic.users.mock.PhonesData.getPhonesUser001())
            .build());
    }


    public static Optional<User> createUser002() {
        User user = new User();
        user.setName(NAME_USER_002 + " " + LAST_NAME_USER_002);
        user.setEmail(EMAIL_USER_002);
        user.setPassword(PASSWORD_USER_002);
        user.setPhones(com.boris.globallogic.users.mock.PhonesData.getPhonesUser002());
        return Optional.of(user);
    }

    public static Optional<User> getUser002() {
        User user = new User();
        user.setId(ID_USER_002);
        user.setName(NAME_USER_002 + " " + LAST_NAME_USER_002);
        user.setEmail(EMAIL_USER_002);
        user.setPassword(PASSWORD_USER_002);
        user.setToken(TOKEN_USER_002);
        user.setIsActive(true);
        user.setPhones(com.boris.globallogic.users.mock.PhonesData.getPhonesUser002());
        return Optional.of(user);
    }

    public static Optional<UserDTO> getUserDTO002() {
        return Optional.of(UserDTO.builder()
                .withName(NAME_USER_002 + " " + LAST_NAME_USER_002)
                .withEmail(EMAIL_USER_002)
                .withPhones(PhonesData.getPhonesUser002())
                .build());
    }

    public static List<User> findAllUsersIsActive() {
        return Arrays.asList(getUser001().get(), getUser002().get());
    }
    public static List<UserDTO> findAllUsers() {
        return Arrays.asList(getUserDTO001().get(), getUserDTO002().get());
    }

}
