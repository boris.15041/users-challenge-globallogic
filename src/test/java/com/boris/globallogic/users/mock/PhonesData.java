package com.boris.globallogic.users.mock;


import com.boris.globallogic.users.dto.PhoneUnregisteredDTO;
import com.boris.globallogic.users.models.entity.Phone;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PhonesData {

    public static Optional<Phone> createPhone001() {
        return Optional.of(new Phone(1L, "3884727442", "4640", "4600"));
    }

    public static Optional<Phone> createPhone002() {
        return Optional.of(new Phone(2L, "3884659263", "4626", "4628"));
    }

    public static Optional<Phone> createPhone003() {
        return Optional.of(new Phone(3L, "3885456952", "4619", "4623"));
    }

    public static Optional<Phone> createPhone004() {
        return Optional.of(new Phone(4L, "3884265987", "4629", "4611"));
    }

    public static Optional<Phone> createPhone005() {
        return Optional.of(new Phone(5L, "3884159357", "4699", "4613"));
    }

    public static List<Phone> getPhonesUser001() {
        return Arrays.asList(createPhone001().get(), createPhone002().get());
    }

    public static List<Phone> getPhonesUser002() {
        return Arrays.asList(createPhone003().get(), createPhone004().get(), createPhone005().get());
    }

    public static List<PhoneUnregisteredDTO> getPhonesUnregisteredDTO001() {
        return Arrays.asList(
                PhoneUnregisteredDTO.builder()
                    .withNumber("3884727442")
                    .withCityCode("4619")
                    .withCountryCode("4618")
                    .build(),
                PhoneUnregisteredDTO.builder()
                        .withNumber("3884979616")
                        .withCityCode("4655")
                        .withCountryCode("4644")
                        .build()
        );
    }

}
