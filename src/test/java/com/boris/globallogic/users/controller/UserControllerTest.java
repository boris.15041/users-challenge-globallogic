package com.boris.globallogic.users.controller;

import static com.boris.globallogic.users.mock.UsersData.*;

import com.boris.globallogic.users.mock.PhonesData;
import com.boris.globallogic.users.services.IUserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserService userService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testFindUserById() throws Exception {
        when(userService.findById(ID_USER_001)).thenReturn(getUserDTO001());

        Map<String, Object> response = new HashMap<>();
        response.put("name", NAME_USER_001 + " " + LAST_NAME_USER_001);
        response.put("email", EMAIL_USER_001);
        response.put("phones", PhonesData.getPhonesUser001());

        mockMvc.perform(get("/api/users/" + ID_USER_001).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(NAME_USER_001 + " " + LAST_NAME_USER_001))
                .andExpect(jsonPath("$.email").value(EMAIL_USER_001))
                .andExpect(content().json(objectMapper.writeValueAsString(response)));

        verify(userService).findById(ID_USER_001);
    }

}