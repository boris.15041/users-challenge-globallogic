package com.boris.globallogic.users.models.repository;

import static com.boris.globallogic.users.mock.UsersData.*;

import com.boris.globallogic.users.models.entity.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserDAOTest {

    public final String EMAIL_USER = "example@gmail.com";

    public final String NAME_USER_NEW = "Agustin Emiliano Romero";
    public final String EMAIL_USER_NEW = "agustin@gmail.com";
    public final String PASSWORD_USER_NEW = "662asDD2";
    public final String TOKEN_USER_NEW = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaXNzIjoiZ2xvYmFsTG9naWMiLCJleHAiOjkwLCJpYXQiOjE2MjcyMTQ4ODUsImVtYWlsIjoicm91OXMzQGdtYWlsLmNvbSIsImp0aSI6IjFkZDM4NDM3LTJiNzEtNGQ4Yy04ZjhiLTNhZWUxM2QwOWIwOSJ9.Ym5KAV7W8qs4mUYuBK8dPmCGl_ikc4Rdn5zGrxWRf2k";
    public final LocalDateTime CREATED_USER_NEW = LocalDateTime.of(2021, 07, 23, 20, 15, 30);
    public final LocalDateTime MODIFIED_USER_NEW = LocalDateTime.of(2021, 07, 24, 22, 16, 31);

    @Autowired
    private UserDAO userDAO;

    @Test
    @Order(1)
    void testFindById() {
        Optional<User> user = userDAO.findById(ID_USER_001);
        assertTrue(user.isPresent());
        assertEquals(ID_USER_001, user.get().getId());
    }

    @Test
    @Order(2)
    void testFindByEmail() {
        Optional<User> user = userDAO.findByEmail(EMAIL_USER_002);
        assertTrue(user.isPresent());
        assertEquals(EMAIL_USER_002, user.get().getEmail());
    }

    @Test
    @Order(3)
    void testFindByEmailThrowException() {
        Optional<User> user = userDAO.findByEmail(EMAIL_USER);
        assertThrows(NoSuchElementException.class, () -> {
            user.get();
        });
        assertFalse(user.isPresent());
    }

    @Test
    @Order(4)
    void testFindAll() {
        List<User> users = userDAO.findAllByIsActiveIsTrue();
        assertFalse(users.isEmpty());
        assertEquals(2, users.size());
    }

    @Test
    @Order(5)
    void testSaveUser() {
        User user = new User();
        user.setName(NAME_USER_NEW);
        user.setEmail(EMAIL_USER_NEW);
        user.setPassword(PASSWORD_USER_NEW);
        user.setToken(TOKEN_USER_NEW);
        user.setCreated(CREATED_USER_NEW);
        user.setModified(MODIFIED_USER_NEW);
        user.setIsActive(true);

        user = userDAO.save(user);

        assertNotNull(user.getId());
        assertEquals(NAME_USER_NEW, user.getName());
        assertEquals(EMAIL_USER_NEW, user.getEmail());
    }

}