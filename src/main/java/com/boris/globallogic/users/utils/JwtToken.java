package com.boris.globallogic.users.utils;

import org.json.JSONObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JwtToken {

    private static final String SECRET_KEY = "SECRET";
    private static final String ISSUER = "globalLogic";
    private static final String JWT_HEADER = "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";
    public static final Integer EXPIRES = 90;
    public static final String SUB = "1234567890";
    private JSONObject payload = new JSONObject();
    private String signature;
    private String encodedHeader;

    private JwtToken() {
        encodedHeader = encode(new JSONObject(JWT_HEADER));
    }

    public JwtToken(String  email, LocalDateTime lastLogin) {
        this();
        payload.put("sub", SUB);
        payload.put("email", email);
        payload.put("lastLogin", lastLogin);
        payload.put("exp", EXPIRES);
        payload.put("iat", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        payload.put("iss", ISSUER);
        payload.put("jti", UUID.randomUUID().toString());
        signature = hmacSha256(encodedHeader + "." + encode(payload), SECRET_KEY);
    }

    @Override
    public String toString() {
        return encodedHeader + "." + encode(payload) + "." + signature;
    }

    private static String encode(JSONObject obj) {
        return encode(obj.toString().getBytes(StandardCharsets.UTF_8));
    }

    private static String encode(byte[] bytes) {
        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }

    public static JSONObject getPayloadFromJWT(String token) throws Exception {
        String[] parts = token.split("\\.");
        JSONObject payload = new JSONObject(decode(parts[1]));
        if (payload.getLong("exp") > (System.currentTimeMillis() / EXPIRES)) {
            throw new Exception("Token is expired");
        }
        return payload;
    }

    private static String decode(String encodedString) {
        return new String(Base64.getUrlDecoder().decode(encodedString));
    }

    private String hmacSha256(String data, String secret) {
        try {
            byte[] hash = secret.getBytes(StandardCharsets.UTF_8);
            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
            sha256Hmac.init(secretKey);
            byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return encode(signedBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            Logger.getLogger(JwtToken.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }
}
