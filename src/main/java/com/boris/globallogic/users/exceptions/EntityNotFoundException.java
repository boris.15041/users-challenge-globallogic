package com.boris.globallogic.users.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Class<?> entity, Object value) {
        this("Not found entity [" + entity.getSimpleName() + "] with value = [" + value + "]");
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

}
