package com.boris.globallogic.users.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestController
@RestControllerAdvice
@Slf4j
public class ResponseExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ExceptionResponse handleAll(Exception ex) {
        return constructException(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ExceptionResponse handlerIllegalArgumentException(ConstraintViolationException ex) {
        return constructException(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ExceptionResponse handlerBadRequestException(BadRequestException ex) {
        return constructException(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UniqueEmailException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final ExceptionResponse handleUniqueEmailException(UniqueEmailException ex) {
        return constructException(ex, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public final ExceptionResponse handleEntityNotFoundException(EntityNotFoundException exception) {
        return constructException(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EntityAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public final ExceptionResponse handlerEntityAlreadyExistsException(EntityAlreadyExistsException ex) {
        return constructException(ex, HttpStatus.CONFLICT);
    }

    private ExceptionResponse constructException(Exception ex, HttpStatus httpStatus) {
        log.debug("ERROR: " + ex.getClass().getName(), ex);
        log.error("ERROR: " + ex.getClass().getName() + ". Message: " + ex.getMessage());
        if (ex instanceof ConstraintViolationException) {
            return new ExceptionResponse(((ConstraintViolationException) ex).getConstraintViolations().stream().findFirst().get().getMessage(), httpStatus);
        }
        return new ExceptionResponse(ex.getMessage(), httpStatus);
    }
}
