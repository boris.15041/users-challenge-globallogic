package com.boris.globallogic.users.dto;

import com.boris.globallogic.users.models.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserUnregisteredDTO {

    private String name;

    private String email;

    @Size(min = 8, max = 12, message = "It must have only one capital letter and only two numbers (not necessarily\n" +
            "consecutive), in a combination of lowercase letters, maximum length of 12 and minimum 8")
    private String password;

    private List<PhoneUnregisteredDTO> phones = new ArrayList<>();

    public static User userUnregisteredDTO2User(UserUnregisteredDTO userUnregisteredDTO) {
        User newUser = new User();
        newUser.setName(userUnregisteredDTO.getName());
        newUser.setEmail(userUnregisteredDTO.getEmail());
        newUser.setIsActive(true);
        newUser.setPhones(userUnregisteredDTO.getPhones().stream()
                .map(phoneDTO -> PhoneUnregisteredDTO.phoneUnregisteredDTO2Phone(phoneDTO))
                .collect(Collectors.toList())
        );
        return newUser;
    }

}
