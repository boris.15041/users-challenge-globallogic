package com.boris.globallogic.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDateTime;

import static com.boris.globallogic.users.utils.ConstantsUtils.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSuccessDTO {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime created;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime lastLogin;

    private String token;

    private Boolean isActive;

}
