package com.boris.globallogic.users.dto;

import com.boris.globallogic.users.models.entity.Phone;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

import static com.boris.globallogic.users.utils.ConstantsUtils.FORMATTER_DATE;

@Getter
@Setter
@Builder(setterPrefix = "with")
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserLoggedDTO {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime created;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMATTER_DATE)
    private LocalDateTime lastLogin;

    private String token;

    private Boolean isActive;

    private String name;

    private String email;

    private String password;

    private List<Phone> phones;

}
