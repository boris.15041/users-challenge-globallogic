package com.boris.globallogic.users.dto;

import com.boris.globallogic.users.models.entity.Phone;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private String id;

    private String name;

    private String email;

    private List<Phone> phones;

}
