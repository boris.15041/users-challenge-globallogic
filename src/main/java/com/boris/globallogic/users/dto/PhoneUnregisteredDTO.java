package com.boris.globallogic.users.dto;

import com.boris.globallogic.users.models.entity.Phone;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PhoneUnregisteredDTO {

    private String number;

    private String cityCode;

    private String countryCode;

    public static Phone phoneUnregisteredDTO2Phone(PhoneUnregisteredDTO phoneUnregisteredDTO) {
        Phone phone = new Phone();
        phone.setNumber(phoneUnregisteredDTO.getNumber());
        phone.setCityCode(phoneUnregisteredDTO.getCityCode());
        phone.setCountryCode(phoneUnregisteredDTO.getCountryCode());
        return phone;
    }
}
