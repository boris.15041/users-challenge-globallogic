package com.boris.globallogic.users.dto;

import com.boris.globallogic.users.models.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRegisteredDTO {

    private String id;

    private String name;

    private String email;

    @Size(min = 8, max = 72, message = "It must have only one capital letter and only two numbers (not necessarily\n" +
            "consecutive), in a combination of lowercase letters, maximum length of 12 and minimum 8")
    private String password;

    private List<PhoneUnregisteredDTO> phones;

    public static User userRegisteredDTO2User(UserRegisteredDTO userRegisteredDTO) {
        User user = new User();
        user.setId(userRegisteredDTO.getId());
        user.setName(userRegisteredDTO.getName());
        user.setIsActive(true);
        user.setEmail(userRegisteredDTO.getEmail());
        user.setPhones(userRegisteredDTO.getPhones().stream()
                .map(phoneDTO -> PhoneUnregisteredDTO.phoneUnregisteredDTO2Phone(phoneDTO))
                .collect(Collectors.toList())
        );
        return user;
    }

}
