package com.boris.globallogic.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class UsersChallengeGloballogicApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersChallengeGloballogicApplication.class, args);
    }

}
