package com.boris.globallogic.users.services;

import com.boris.globallogic.users.dto.*;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<UserDTO> findAll();

    Optional<UserDTO> findById(String id);

    Optional<UserSuccessDTO> save(UserUnregisteredDTO userUnregisteredDTO);

    Optional<UserSuccessDTO> update(UserRegisteredDTO userRegisteredDTO);

    void deleteById(String id);

    Optional<UserLoggedDTO> login(String email);

}
