package com.boris.globallogic.users.services.impl;

import com.boris.globallogic.users.dto.*;
import com.boris.globallogic.users.exceptions.BadRequestException;
import com.boris.globallogic.users.exceptions.ConstantsException;
import com.boris.globallogic.users.exceptions.EntityNotFoundException;
import com.boris.globallogic.users.exceptions.UniqueEmailException;
import com.boris.globallogic.users.models.entity.User;
import com.boris.globallogic.users.models.repository.UserDAO;
import com.boris.globallogic.users.services.IUserService;
import com.boris.globallogic.users.utils.JwtToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {

    private final UserDAO userDAO;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserDAO userDAO, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDAO = userDAO;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDTO> findAll() {
        return userDAO.findAllByIsActiveIsTrue().stream().collect(ArrayList::new, (list, element) -> {
            list.add(UserDTO.builder()
                    .withId(element.getId())
                    .withName(element.getName())
                    .withEmail(element.getEmail())
                    .withPhones(element.getPhones())
                    .build());
        }, ArrayList::addAll);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserDTO> findById(String id) {
        Optional<User> userFound = Optional.of(userDAO.findById(id).orElseThrow(() -> new EntityNotFoundException(User.class, id)));
        return userFound.map(user -> UserDTO.builder()
                .withName(user.getName())
                .withEmail(user.getEmail())
                .withPhones(user.getPhones())
                .build()
        );
    }

    public Optional<User> save(User user) {
        return Optional.of(userDAO.save(user));
    }

    @Override
    @Transactional
    public Optional<UserSuccessDTO> save(UserUnregisteredDTO userUnregisteredDTO) {
        userDAO.findByEmail(userUnregisteredDTO.getEmail())
                .ifPresent(user -> { throw new UniqueEmailException(user.getEmail());});
        User newUser = UserUnregisteredDTO.userUnregisteredDTO2User(userUnregisteredDTO);
        newUser.setToken(new JwtToken(newUser.getEmail(), newUser.getLastLogin()).toString());
        newUser.setPassword(bCryptPasswordEncoder.encode(userUnregisteredDTO.getPassword()));
        newUser = userDAO.save(newUser);
        return Optional.of(UserSuccessDTO.builder()
                .withId(newUser.getId())
                .withCreated(newUser.getCreated())
                .withLastLogin(LocalDateTime.now())
                .withToken(newUser.getToken())
                .withIsActive(newUser.getIsActive())
                .build());
    }

    @Override
    @Transactional
    public Optional<UserSuccessDTO> update(UserRegisteredDTO userRegisteredDTO) {
        if (userRegisteredDTO.getId() == null) {
            throw new BadRequestException("Id is required to update the User");
        }
        userDAO.findById(userRegisteredDTO.getId())
                .orElseThrow(() -> new EntityNotFoundException(ConstantsException.USER_NOT_FOUND_EXCEPTION + userRegisteredDTO.getId()));
        User updateUser = UserRegisteredDTO.userRegisteredDTO2User(userRegisteredDTO);
        updateUser.setToken(new JwtToken(updateUser.getEmail(), updateUser.getLastLogin()).toString());
        updateUser.setPassword(bCryptPasswordEncoder.encode(userRegisteredDTO.getPassword()));
        return save(updateUser)
                .map(userUpdate -> UserSuccessDTO.builder()
                    .withId(userUpdate.getId())
                    .withCreated(userUpdate.getCreated())
                    .withLastLogin(LocalDateTime.now())
                    .withToken(userUpdate.getToken())
                    .withIsActive(userUpdate.getIsActive())
                    .build());
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        User userFound = userDAO.findById(id).orElseThrow(() -> new EntityNotFoundException(ConstantsException.USER_NOT_FOUND_EXCEPTION + id));
        userFound.setIsActive(false);
        userDAO.save(userFound);
    }

    @Override
    public Optional<UserLoggedDTO> login(String email) {
        User userLoggedIn = userDAO.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("There is no user with the email: " + email));
        userLoggedIn.setLastLogin(LocalDateTime.now());
        userLoggedIn.setToken(new JwtToken(userLoggedIn.getEmail(), userLoggedIn.getLastLogin()).toString());
        return save(userLoggedIn)
                .map(userUpdate -> UserLoggedDTO.builder()
                        .withId(userUpdate.getId())
                        .withCreated(userUpdate.getCreated())
                        .withLastLogin(LocalDateTime.now())
                        .withToken(userUpdate.getToken())
                        .withIsActive(userUpdate.getIsActive())
                        .withName(userUpdate.getName())
                        .withEmail(userUpdate.getEmail())
                        .withPassword(userUpdate.getPassword())
                        .withPhones(userUpdate.getPhones())
                        .build());
    }

}
