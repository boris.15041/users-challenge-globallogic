package com.boris.globallogic.users.controller;

import com.boris.globallogic.users.dto.*;
import com.boris.globallogic.users.exceptions.EntityNotFoundException;
import com.boris.globallogic.users.services.IUserService;
import com.boris.globallogic.users.utils.JwtToken;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll() {
        log.info("Get all users");
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable String id) {
        log.info("Get user by id {}", id);
        return ResponseEntity.ok(userService.findById(id).get());
    }

    @PostMapping("/sign-up")
    public ResponseEntity<UserSuccessDTO> saveUser(@Valid @RequestBody UserUnregisteredDTO userUnregistered) {
        log.info("Save user: {}", userUnregistered);
        return new ResponseEntity<>(userService.save(userUnregistered).get(), HttpStatus.CREATED);
    }

    @GetMapping("/login")
    public ResponseEntity<UserLoggedDTO> login(@RequestParam String token) throws Exception {
        JSONObject payload = JwtToken.getPayloadFromJWT(token);
        String email = payload.getString("email");
        log.info("Login user with email: " + email);
        return ResponseEntity.accepted().body(userService.login(email).get());
    }

    @PutMapping
    public ResponseEntity<UserSuccessDTO> updateUser(@Valid @RequestBody UserRegisteredDTO userRegisteredDTO) {
        log.info("Update user: {}", userRegisteredDTO);
        return ResponseEntity.ok(userService.update(userRegisteredDTO).orElseThrow(() -> new EntityNotFoundException("User not found")));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUserById(@PathVariable String id) {
        log.info("Delete user with id: {}", id);
        userService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
