package com.boris.globallogic.users.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "PHONES")
@Data
public class Phone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    @Size(max = 15, message = "Number phone cannot contain more than 15 characters")
    private String number;

    @Column(name = "CITY_CODE", length = 10)
    @Size(max = 10, message = "City code cannot contain more than 10 characters")
    private String cityCode;

    @Column(name = "COUNTRY_CODE", length = 10)
    @Size(max = 10, message = "Country code cannot contain more than 10 characters")
    private String countryCode;

    @JsonIgnore
    @Column(name = "USER_ID", length = 36)
    private String userId;

    public Phone() {
    }

    public Phone(
            Long id,
            @Size(max = 15, message = "Number phone cannot contain more than 15 characters") String number,
            @Size(max = 10, message = "City code cannot contain more than 10 characters") String cityCode,
            @Size(max = 10, message = "Country code cannot contain more than 10 characters") String countryCode
    ) {
        this.id = id;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }
}
