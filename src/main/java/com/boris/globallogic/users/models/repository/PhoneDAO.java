package com.boris.globallogic.users.models.repository;

import com.boris.globallogic.users.models.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneDAO extends JpaRepository<Phone, Long> {
}
