package com.boris.globallogic.users.models.repository;

import com.boris.globallogic.users.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDAO extends JpaRepository<User, String> {

    Optional<User> findByEmail(String email);

    List<User> findAllByIsActiveIsTrue();

}
